<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img src="{{ asset('images/logo.png')}}" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./"><img src="{{  asset('images/logo2.png')}}" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('dashboard') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <h3 class="menu-title">UI elements</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Components</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-puzzle-piece"></i><a href="{{ route('ui_bottons') }}">Buttons</a></li>
                        <li><i class="fa fa-id-badge"></i><a href="{{ route('ui-badges') }}">Badges</a></li>
                        <li><i class="fa fa-bars"></i><a href="{{ route('ui-tabs') }}">Tabs</a></li>
                        <li><i class="fa fa-share-square-o"></i><a href="{{ route('ui-social_buttons') }}">Social
                                Buttons</a></li>
                        <li><i class="fa fa-id-card-o"></i><a href="{{ route('ui-cards') }}">Cards</a></li>
                        <li><i class="fa fa-exclamation-triangle"></i><a href="{{ route('ui-alerts') }}">Alerts</a></li>
                        <li><i class="fa fa-spinner"></i><a href="{{ route('ui-progress_bars') }}">Progress Bars</a>
                        </li>
                        <li><i class="fa fa-fire"></i><a href="{{ route('ui-modals') }}">Modals</a></li>
                        <li><i class="fa fa-book"></i><a href="{{ route('ui-switches') }}">Switches</a></li>
                        <li><i class="fa fa-th"></i><a href="{{ route('ui-grids') }}">Grids</a></li>
                        <li><i class="fa fa-file-word-o"></i><a href="{{ route('ui-typography') }}">Typography</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children active dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Tables</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-table"></i><a href="{{ route('ui_table_basic') }}">Basic Table</a></li>
                        <li><i class="fa fa-table"></i><a href="{{ route('ui_table_data') }}">Data Table</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Forms</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="{{ route('ui_forms_basic') }}">Basic Form</a>
                        </li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{ route('ui_forms_advanced') }}">Advanced
                                Form</a></li>
                    </ul>
                </li>

                <h3 class="menu-title">Icons</h3><!-- /.menu-title -->

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-fort-awesome"></i><a href="{{ route('icons_fontawesom') }}">Font Awesome</a>
                        </li>
                        <li><i class="menu-icon ti-themify-logo"></i><a href="{{ route('icons_themify') }}">Themefy Icons</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('icons_widgets') }}"> <i class="menu-icon ti-email"></i>Widgets </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-line-chart"></i><a href="{{ route('charts-chartjs') }}">Chart JS</a></li>
                        <li><i class="menu-icon fa fa-area-chart"></i><a href="{{ route('charts-flot') }}">Flot Chart</a></li>
                        <li><i class="menu-icon fa fa-pie-chart"></i><a href="{{ route('charts-peity') }}">Peity Chart</a></li>
                    </ul>
                </li>

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-map-o"></i><a href="{{ route('maps-gmap') }}">Google Maps</a></li>
                        <li><i class="menu-icon fa fa-street-view"></i><a href="{{ route('maps-vector') }}">Vector Maps</a></li>
                    </ul>
                </li>
                <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('pages_login') }}">Login</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('pages_register') }}">Register</a>
                        </li>
                        <li><i class="menu-icon fa fa-paper-plane"></i><a href="{{ route('pages_forgetPass') }}">Forget
                                Pass</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>