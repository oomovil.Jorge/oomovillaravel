<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::group(['prefix' => 'ui_elements'], function () {

    Route::group(['prefix' => 'components'], function () {
        Route::get('/buttons', function () {
            return view('ui_elements.components.ui-buttons');
        })->name('ui_bottons');
        Route::get('/badges', function () {
            return view('ui_elements.components.ui-badges');
        })->name('ui-badges');
        Route::get('/tabs', function () {
            return view('ui_elements.components.ui-tabs');
        })->name('ui-tabs');
        Route::get('/social_buttons', function () {
            return view('ui_elements.components.ui-social-buttons');
        })->name('ui-social_buttons');
        Route::get('/cards', function () {
            return view('ui_elements.components.ui-cards');
        })->name('ui-cards');
        Route::get('/alerts', function () {
            return view('ui_elements.components.ui-alerts');
        })->name('ui-alerts');
        Route::get('/progress_bars', function () {
            return view('ui_elements.components.ui-progressbar');
        })->name('ui-progress_bars');
        Route::get('/modals', function () {
            return view('ui_elements.components.ui-modals');
        })->name('ui-modals');
        Route::get('/switches', function () {
            return view('ui_elements.components.ui-switches');
        })->name('ui-switches');
        Route::get('/grids', function () {
            return view('ui_elements.components.ui-grids');
        })->name('ui-grids');
        Route::get('/typography', function () {
            return view('ui_elements.components.ui-typography');
        })->name('ui-typography');
    });

    Route::group(['prefix' => 'tables'], function () {
        Route::get('/tablas_simple', function () {
            return view('ui_elements.tables.tables_basic');
        })->name('ui_table_basic');
        Route::get('/tablas_data', function () {
            return view('ui_elements.tables.tables_data');
        })->name('ui_table_data');
    });

    Route::group(['prefix' => 'forms'], function () {
        Route::get('/forms_advanced', function () {
            return view('ui_elements.forms.forms-advanced');
        })->name('ui_forms_advanced');
        Route::get('/forms_basic', function () {
            return view('ui_elements.forms.forms-basic');
        })->name('ui_forms_basic');
    });

});


Route::group(['prefix' => 'icons'], function () {

    Route::group(['prefix' => 'icons'], function () {
        Route::get('/font_Awesome', function () {
            return view('icons.icons.font-fontawesome'); })->name('icons_fontawesom');
        Route::get('/themefy_icons', function () {
            return view('icons.icons.font-themify'); })->name('icons_themify');
    });

    Route::get('/widgets', function () {
        return view('icons.widgets');
    })->name('icons_widgets');


    Route::group(['prefix' => 'charts'], function () {
        Route::get('/charts_js', function () {
            return view('icons.charts.charts-chartjs'); })->name('charts-chartjs');
        Route::get('/flot_chart', function () {
            return view('icons.charts.charts-flot'); })->name('charts-flot');
        Route::get('/peity_chart', function () {
            return view('icons.charts.charts-peity'); })->name('charts-peity');
    });

    Route::group(['prefix' => 'maps'], function () {
        Route::get('/google_Maps', function () {
            return view('icons.maps.maps-gmap'); })->name('maps-gmap');
        Route::get('/Vector_Maps', function () {
            return view('icons.maps.maps-vector'); })->name('maps-vector');
    });

});


Route::group(['prefix' => 'extras'], function () {
    Route::group(['prefix' => 'pages'], function () {
        Route::get('/login', function () {
            return view('extras.pages.page-login');
        })->name('pages_login');
        Route::get('/register', function () {
            return view('extras.pages.page-register');
        })->name('pages_register');
        Route::get('/forget_pass', function () {
            return view('extras.pages.pages-forget');
        })->name('pages_forgetPass');
    });
});

